package screen;

import main.GamePanel;
import java.awt.Rectangle;

import characters.Player;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.BasicStroke;

public class HealthBar {
	
	GamePanel gp;
	Player player;
	final int positionX;
	final int positionY;
	Rectangle maxHealthBar;
	Rectangle currentHealthBar;
	
	public HealthBar(GamePanel gp, Player player) {
		this.gp = gp;
		this.player = player;
		positionX = gp.screenWidth / 4 - 50;
		positionY = gp.screenHeight - 75;
		maxHealthBar = new Rectangle(positionX + 230, positionY - 27,gp.screenWidth / 2, gp.tileSize / 2);
		currentHealthBar = new Rectangle(positionX + 230, positionY - 27,gp.screenWidth / 2, gp.tileSize / 2);;

	}
	
	public void update() {
		player.currentHealth = 40;
		double width = maxHealthBar.getWidth();
		int newWidth = (int)(width) * player.currentHealth / player.maxHealth;
		currentHealthBar.width = newWidth;
	}
	
	public void draw(Graphics2D g2) {

		String health = String.format("HP: %d / %d", player.currentHealth, player.maxHealth);
		g2.setColor(Color.white);
		g2.setFont(new Font("Arial", Font.BOLD, 30));
		g2.drawString(health, positionX, positionY);
		
		g2.setColor(Color.black);
		g2.setStroke(new BasicStroke(3));
		g2.draw(maxHealthBar);
		
		g2.setStroke(new BasicStroke(1));
		g2.draw(currentHealthBar);
		if (player.currentHealth <= player.maxHealth * 0.2)
			g2.setColor(Color.red);
		else if (player.currentHealth < player.maxHealth * 0.5)
			g2.setColor(Color.yellow);
		else
			g2.setColor(Color.green);
		g2.fill(currentHealthBar);
		
	}
	
}
