package screen;

import main.GamePanel;
import java.awt.Rectangle;

import characters.Player;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.BasicStroke;

public class LevelBar {
	
	GamePanel gp;
	Player player;
	final int positionX;
	final int positionY;
	Rectangle maxExpBar;
	Rectangle currentExpBar;
	
	public LevelBar(GamePanel gp, Player player) {
		this.gp = gp;
		this.player = player;
		positionX = gp.screenWidth / 4 - 50;
		positionY = gp.screenHeight - 40;
		maxExpBar = new Rectangle(positionX + 230, positionY - 20,gp.screenWidth / 2, gp.tileSize / 4);
		currentExpBar = new Rectangle(positionX + 230, positionY - 20,gp.screenWidth / 2, gp.tileSize / 4);;

	}
	
	public void update() {
		player.currentExp = 4;
		double width = maxExpBar.getWidth();
		int newWidth = (int)(width) * player.currentExp / player.maxExp;
		currentExpBar.width = newWidth;
	}
	
	public void draw(Graphics2D g2) {

		String exp = String.format("Exp: %d / %d", player.currentExp, player.maxExp);
		String level = String.format("Level %d", player.level);
		g2.setColor(Color.white);
		g2.setFont(new Font("Arial", Font.BOLD, 30));
		g2.drawString(exp, positionX, positionY);
		
		g2.setFont(new Font("Arial", Font.BOLD, 40));
		g2.drawString(level, 20, gp.screenHeight - 50);
		
		g2.setColor(Color.black);
		g2.setStroke(new BasicStroke(3));
		g2.draw(maxExpBar);
		
		g2.setStroke(new BasicStroke(1));
		g2.draw(currentExpBar);
		g2.setColor(Color.cyan);
		g2.fill(currentExpBar);
		
	}
	
}
