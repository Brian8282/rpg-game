//Used videos from https://www.youtube.com/c/RyiSnow to help create the game window, tiles, main character
package main;

import characters.Character;

public class CollisionChecker {
	
	GamePanel gp;
	public int PlayerTileTypeNum; //what type of tile the player is currently on
	public CollisionChecker(GamePanel gp) {
		
		this.gp = gp;
		PlayerTileTypeNum = 0;
	}
	

	//Just checks for walls
	public void CheckTile(Character character) {
		
		int characterTileTypeNum; //what type of tile the character is currently on
		int mapColNum = character.x / gp.tileSize;
		int mapRowNum = character.y / gp.tileSize;

		if (character.direction == "up") {
			mapRowNum = (character.y - character.speed) / gp.tileSize;
			characterTileTypeNum = gp.tilemanager.mapTileNum[mapColNum][mapRowNum];
			if (gp.tilemanager.tile[characterTileTypeNum].collision == true) {
				character.collisionOn = true;
			}
			if (gp.tilemanager.tile[characterTileTypeNum].teleporter == true) {
				String map = gp.tilemanager.chooseMap(gp.tilemanager.tile[characterTileTypeNum].teleporterLocation);
				gp.tilemanager.loadMap(map);
			}
		}
		else if (character.direction == "down") {
			mapRowNum = (character.y + character.speed) / gp.tileSize;
			characterTileTypeNum = gp.tilemanager.mapTileNum[mapColNum][mapRowNum];
			if (gp.tilemanager.tile[characterTileTypeNum].collision == true) {
				character.collisionOn = true;
			}
			if (gp.tilemanager.tile[characterTileTypeNum].teleporter == true) {
				String map = gp.tilemanager.chooseMap(gp.tilemanager.tile[characterTileTypeNum].teleporterLocation);
				gp.tilemanager.loadMap(map);
			}
		}
		else if (character.direction == "left") {
			mapColNum = (character.x - character.speed) / gp.tileSize;
			characterTileTypeNum = gp.tilemanager.mapTileNum[mapColNum][mapRowNum];
			if (gp.tilemanager.tile[characterTileTypeNum].collision == true) {
				character.collisionOn = true;
			}
			if (gp.tilemanager.tile[characterTileTypeNum].teleporter == true) {
				String map = gp.tilemanager.chooseMap(gp.tilemanager.tile[characterTileTypeNum].teleporterLocation);
				gp.tilemanager.loadMap(map);
			}
		}
		else {   //right
			mapColNum = (character.x + character.speed) / gp.tileSize;
			characterTileTypeNum = gp.tilemanager.mapTileNum[mapColNum][mapRowNum];
			if (gp.tilemanager.tile[characterTileTypeNum].collision == true) {
				character.collisionOn = true;
			}
			if (gp.tilemanager.tile[characterTileTypeNum].teleporter == true) {
				String map = gp.tilemanager.chooseMap(gp.tilemanager.tile[characterTileTypeNum].teleporterLocation);
				gp.tilemanager.loadMap(map);
			}
		}
		
	}
	
	
}

