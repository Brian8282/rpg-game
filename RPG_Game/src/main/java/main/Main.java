package main;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		
		JFrame window = new JFrame(); //game window
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false); //whether user can change window size
		window.setTitle("RPG GAME");
		
		GamePanel gamePanel = new GamePanel();
		window.add(gamePanel);
		window.pack(); //size the window so everything is visible
		
		window.setLocationRelativeTo(null); //put window center of screen
		window.setVisible(true);
		
		gamePanel.startGameThread(); //calls function from gamePanel to start the main thread of the game

	}

}
