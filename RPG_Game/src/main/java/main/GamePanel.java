package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import characters.Player;
import screen.HealthBar;
import screen.LevelBar;

public class GamePanel extends JPanel implements Runnable{
	
	//Screen settings
	public final int tileSize = 60; 
	public final int maxScreenCol = 15;
	public final int maxScreenRow = 12;
	public final int gameScreenCol = 15;
	public final int gameScreenRow = 10;
	public final int screenWidth = tileSize * maxScreenCol; 
	public final int screenHeight = tileSize * maxScreenRow; //windows screen including bottom bar for lvl,etc.
	public final int gameWidth = tileSize * gameScreenCol; 
	public final int gameHeight = tileSize * gameScreenRow; //not including bottom bar

	
	Thread gameThread;
	KeyHandler keyhandler = new KeyHandler();
	TileManager tilemanager = new TileManager(this);
	CollisionChecker collisionchecker = new CollisionChecker(this);
	Player player = new Player(this, keyhandler, collisionchecker);
	HealthBar healthBar = new HealthBar(this, player);
	LevelBar levelBar = new LevelBar(this, player);
	
	public GamePanel(){
		
		this.setPreferredSize(new Dimension(screenWidth, screenHeight));
		this.setBackground(Color.darkGray);
		this.setDoubleBuffered(true); //improve game rendering performance
		this.addKeyListener(keyhandler);
		this.setFocusable(true);
	}

	//Main thread for the game
	public void startGameThread() {
		
		gameThread = new Thread(this);
		gameThread.start(); //will call run method

	}
	
	public void run() {
		
		double drawInterval = 1000000000 / 8; //how often the screen gets redrawn (game pace)
		double timePassed = 0;
		long lastTime = System.nanoTime(); //1000000000 nanoseconds = 1 second
		long currentTime;
		
		while(gameThread != null) {
			
			currentTime = System.nanoTime();
			
			timePassed += (currentTime - lastTime) / drawInterval;
			
			lastTime = currentTime;
			if (timePassed >= 1) {
				update(); 
				repaint(); //redraws the screen (basically calls paintComponent function)
				timePassed--;
			}	
		}
	}
	
	//objects that get updated
	public void update() {

		player.update();
		healthBar.update();
		levelBar.update();
	}
	
	//Automatically called at start of game
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g); //need this whenever using paintComponent 
		
		Graphics2D g2 = (Graphics2D)g;
		tilemanager.draw(g2); //draw tiles before player so player will be at front
		player.draw(g2);
		healthBar.draw(g2);
		levelBar.draw(g2);
		g2.dispose();
	}
	
}
