//Used videos from https://www.youtube.com/c/RyiSnow to help create the game window, tiles, main character
package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;

public class TileManager {

	public GamePanel gp;
	KeyHandler keyH;
	public Tile[] tile;
	public int mapTileNum[][];
	String[][] worldMap;
	String currentMap;
	
	public TileManager(GamePanel gp) {
		
		this.gp = gp;
		tile = new Tile[10]; //10 types of tiles max
		mapTileNum = new int[gp.gameScreenCol][gp.gameScreenRow];
		getTileImage();
		loadMap("/map/level1.txt");
		worldMap = loadWorldMap();
	}
	
	//loads the tile image
	public void getTileImage() {
		
		try {
			
			//normal tile
			tile[0] = new main.Tile();
			tile[0].image = ImageIO.read(getClass().getResourceAsStream("/images/grass.png"));
			
			//wall
			tile[1] = new main.Tile();
			tile[1].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			tile[1].collision = true;
			
			//teleporter north
			tile[2] = new main.Tile();
			tile[2].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			tile[2].teleporter = true;
			tile[2].teleporterLocation = "north";
			
			//teleporter south
			tile[3] = new main.Tile();
			tile[3].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			tile[3].teleporter = true;
			tile[3].teleporterLocation = "south";
			
			//teleporter west
			tile[4] = new main.Tile();
			tile[4].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			tile[4].teleporter = true;
			tile[4].teleporterLocation = "west";
			
			//teleporter east
			tile[5] = new main.Tile();
			tile[5].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			tile[5].teleporter = true;
			tile[5].teleporterLocation = "east";
			
			//A enemy
			tile[6] = new main.Tile();
			tile[6].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			
			//Another enemy
			tile[7] = new main.Tile();
			tile[7].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			
			//Another enemy
			tile[8] = new main.Tile();
			tile[8].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));
			
			//A boss
			tile[9] = new main.Tile();
			tile[9].image = ImageIO.read(getClass().getResourceAsStream("/images/tree.png"));

		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//puts the values from the level.txt into the mapTileNum array
	public void loadMap(String filePath) {
		
		try {
			
			InputStream is = getClass().getResourceAsStream(filePath);
			BufferedReader file = new BufferedReader(new InputStreamReader(is));
			
			int col = 0;
			int row = 0;
			
			while(col < gp.gameScreenCol && row < gp.gameScreenRow) {
				
				String line = file.readLine();
				
				while(col < gp.gameScreenCol) {
					
					String numbers[] = line.split(" ");
					
					int num = Integer.parseInt(numbers[col]);
					
					mapTileNum[col][row] = num;
					col++;
				}
				
				if (col == gp.gameScreenCol) {
					col = 0;
					row++;
				}
			}
			currentMap = filePath.substring(5, filePath.length() - 4);
			file.close();
			
		} catch(Exception e){
		}
	}
	
	//put the contents of the worldmap.txt file into an array
	public String[][] loadWorldMap(){
		try {
			InputStream is = getClass().getResourceAsStream("/map/worldmap.txt");
			BufferedReader file = new BufferedReader(new InputStreamReader(is));
			int size = 0;
			file.mark(10000);
			while (file.readLine() != null) {
				size++;
			}
			file.reset();
			worldMap = new String[size][3];
			
			for (int x = 0; x < size; x++) {
				String line = file.readLine();
				String words[] = line.split(" ");
				for (int y = 0; y < 3; y++) {
					worldMap[x][y] = words[y];
				}
			}
			
			file.close();
		} catch(Exception e){
		}
		return worldMap;
	}
	
	//choose which map to teleport to when a teleporter is touched
	public String chooseMap(String location) {
		String map = "/map/";
		for (int x = 0; x < worldMap.length; x++) {
			if (currentMap.equals(worldMap[x][0])) {
				if (location.equals(worldMap[x][1])) {
					map += worldMap[x][2];
					break;
				}
			}
		}
		map += ".txt";
		return map;
	}
	
	//draws the tiles (the map)
	public void draw(Graphics2D g2) {
		
		int col = 0;
		int row = 0;
		int x = 0;
		int y = 0;
		
		while(col < gp.gameScreenCol && row < gp.gameScreenRow) {
			
			int tileNum = mapTileNum[col][row];
			
			g2.drawImage(tile[tileNum].image, x, y, gp.tileSize, gp.tileSize, null);
			col++;
			x += gp.tileSize;
			
			if (col == gp.gameScreenCol) {
				col = 0;
				x = 0;
				row++;
				y += gp.tileSize;
			}
		}
	}
	
	
	
	
}
