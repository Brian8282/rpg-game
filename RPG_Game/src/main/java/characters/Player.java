package characters;

import main.GamePanel;
import java.awt.Graphics2D;
import java.io.IOException;
import javax.imageio.ImageIO;
import main.KeyHandler;
import main.CollisionChecker;

public class Player extends Character{
	
	GamePanel gp;
	KeyHandler keyhandler;
	CollisionChecker collisionchecker;
	public int maxExp;
	public int currentExp;
	
	public Player(GamePanel gp, KeyHandler keyhandler, CollisionChecker collisionchecker) {
		
		this.gp = gp;
		this.keyhandler = keyhandler;
		this.collisionchecker = collisionchecker;
		setDefaultValues();
		getPlayerImage();
	}
	
	public void setDefaultValues() {
		//player position is (tileSize,tileSize) by default if not set
		x = gp.tileSize;
		y = gp.tileSize;
		speed = gp.tileSize;
		level = 1;
		currentHealth = 100;
		maxHealth = 100;
		maxExp = 10;
		currentExp = 0;
	}
	
	//set (x,y) coordinate for player
	/*public void setPosition() {
		for (int col = 0; col < gp.maxScreenCol; col++) {
			for (int row = 0; row < gp.maxScreenRow; row++) {
				if (tileM.mapTileNum[col][row] == 8) {
					x = gp.tileSize * col;
					y = gp.tileSize * row;
					break;
				}
			}
		}
	}
	*/
	
	//loads image for the player
	public void getPlayerImage() {
		
		try {
			
			img = ImageIO.read(getClass().getResourceAsStream("/images/pokemonplayer.png"));
			
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	//updates the player position
	public void update() {
		
		if (keyhandler.upPressed == true || keyhandler.downPressed == true || 
				keyhandler.leftPressed == true || keyhandler.rightPressed == true) {
			if (keyhandler.upPressed == true) {
				direction = "up";
			}
			else if (keyhandler.downPressed == true) {
				direction = "down";
			}
			else if (keyhandler.leftPressed == true) {
				direction = "left";
			}
			else {
				direction = "right";
			}
			
			//check tile collision
			collisionOn = false;
			collisionchecker.CheckTile(this); //check to make sure it isn't a wall
			
			//if collision is false, player can move 
			if (collisionOn == false) {
				
				if (direction == "up")
					y -= speed;
				else if (direction == "down")
					y += speed;
				else if (direction == "left")
					x -= speed;
				else if (direction == "right")
					x += speed;
					
			}
		}

	}
	
	//draw the player in the game
	public void draw(Graphics2D g2) {
		
		g2.drawImage(img, x, y, gp.tileSize, gp.tileSize, null);
	}
	
	
	
}



