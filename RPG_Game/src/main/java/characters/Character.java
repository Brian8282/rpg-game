package characters;

import java.awt.image.BufferedImage;

public class Character {
	
	public int x, y;
	public int speed;
	public int currentHealth;
	public int maxHealth;
	public int level;
	public int damage;
	public BufferedImage img;
	public String direction;
	
	public boolean collisionOn = false;
}
